#!/bin/bash

# Commdore's firmware is owned and licensed by Cloanto. The ROMs required by this VICE fork should be obtained as part of Cloanto's C64 Forever Express https://www.c64forever.com/download/
# For convenience Linux users are advised to install C64 Forever using Wine 5.0 or above, which has full MSI installer support (https://www.winehq.org/news/2020012101)
# After installation, locate /users/Public/Documents/CBM Files/Shared/rom/ of your Windows system drive
# Copy the rom directory to the vice-3.5-copyright-compliant directory and run this file

#C64 firmware
cp rom/c-64-kernal.rom data/C64/kernal
cp rom/c-64-chargen.rom data/C64/chargen
cp rom/c-64-basic.rom data/C64/basic
cp rom/c-64-kernal-jp.rom data/C64/jpkernal
cp rom/c-64-chargen-jp.rom data/C64/jpchrgen

#C128 firmware
cp rom/c-128-basic-64.rom data/C128/basic64
cp rom/c-128-basic-hi.rom data/C128/basichi
cp rom/c-128-basic-lo.rom data/C128/basiclo
cp rom/c-128-chargen.rom data/C128/chargen
cp rom/c-128-chargen-ch.rom data/C128/chargch
cp rom/c-128-chargen-de.rom data/C128/chargde
cp rom/c-128-chargen-fr.rom data/C128/chargfr
cp rom/c-128-chargen-no.rom data/C128/chargno
cp rom/c-128-chargen-se.rom data/C128/chargse
cp rom/c-128-kernal-64.rom data/C128/kernal64
cp rom/c-128-kernal.rom data/C128/kernal
cp rom/c-128-kernal-ch.rom data/C128/kernalch
cp rom/c-128-kernal-de.rom data/C128/kernalde
cp rom/c-128-kernal-fi.rom data/C128/kernalfi
cp rom/c-128-kernal-fr.rom data/C128/kernalfr
cp rom/c-128-kernal-it.rom data/C128/kernalit
cp rom/c-128-kernal-no.rom data/C128/kernalno
cp rom/c-128-kernal-se.rom data/C128/kernalse

#VIC20 firmware
cp rom/c-vic20-basic.rom data/VIC20/basic
cp rom/c-vic20-chargen.rom data/VIC20/chargen
cp rom/c-vic20-kernal.rom data/VIC20/kernal

#PLUS4 firmware
cp rom/c-plus4-kernal.rom data/PLUS4/kernal
cp rom/c-16-kernal.rom data/PLUS4/kernal.005
cp rom/c-232-kernal.rom data/PLUS4/kernal.232
cp rom/c-v364-kernal.rom data/PLUS4/kernal.364
cp rom/c-plus4-ext-hi.rom data/PLUS4/3plus1hi
cp rom/c-plus4-ext-lo.rom data/PLUS4/3plus1lo
cp rom/c-plus4-basic.rom data/PLUS4/basic
cp rom/c-v364-kernal.rom data/PLUS4/c2lo.364

#C64DTV firmware
cp rom/c-64-kernal.rom data/C64DTV/kernal #check
cp rom/c-64-chargen.rom data/C64DTV/chargen #check
cp rom/c-64-basic.rom data/C64DTV/basic #check

#SCPU64 firmware
cp rom/c-64-chargen.rom data/SCPU64/chargen #check
cp rom/c-64-chargen-jp.rom data/SCPU64/jpchrgen #check
# Vice's replacement SCPU64 ROM "compatible with the original to avoid distribution problems" is used here so a licensed original is not required.

#CBM-II firmware
cp rom/c-500-basic.rom data/CBM-II/basic.500
cp rom/c-500-chargen.rom data/CBM-II/chargen.500
cp rom/c-500-kernal.rom data/CBM-II/kernal.500
cp rom/c-700-kernal.rom data/CBM-II/kernal
cp rom/c-600-128k-basic.rom data/CBM-II/basic.128
cp rom/c-600-256k-basic.rom data/CBM-II/basic.256
cp rom/c-600-chargen.rom data/CBM-II/chargen.600
cp rom/c-600-kernal.rom data/CBM-II/kernal.600
cp rom/c-700-chargen.rom data/CBM-II/chargen.700
cp rom/c-700-kernal.rom data/CBM-II/kernal.700

#drive firmware
cp rom/c-drive-1541-ii.rom data/DRIVES/d1541II
cp rom/c-drive-1571-c128dcr.rom data/DRIVES/d1571cr
cp rom/c-drive-1001.rom data/DRIVES/dos1001
cp rom/c-drive-1541.rom data/DRIVES/dos1540 #this is a cludge to make it build as we don't have the 1540 firmware. This placeholder will probably break things if used.
cp rom/c-drive-1541.rom data/DRIVES/dos1541
cp rom/c-drive-1551.rom data/DRIVES/dos1551
cp rom/c-drive-1570.rom data/DRIVES/dos1570
cp rom/c-drive-1571.rom data/DRIVES/dos1571
cp rom/c-drive-1581.rom data/DRIVES/dos1581
cp rom/c-drive-2031.rom data/DRIVES/dos2031
cp rom/c-drive-2040.rom data/DRIVES/dos2040
cp rom/c-drive-3040.rom data/DRIVES/dos3040
cp rom/c-drive-4000.rom data/DRIVES/dos4000
cp rom/c-drive-4040.rom data/DRIVES/dos4040
cp rom/c-drive-1581.rom data/DRIVES/dos1581
touch data/DRIVES/dos9000 #dos9000 image is missing from Cloanto set. this is a placeholder and will break horribly if used

#printer firmware
cp rom/c-printer-mps801.rom data/PRINTER/mps801
cp rom/c-printer-mps802.rom data/PRINTER/cbm1526
cp rom/c-printer-mps803.rom data/PRINTER/mps803
cp rom/c-printer-mps803.rom data/PRINTER/nl10-cbm #nl10-cbm printer firmware is created & owned by Star. this is a placeholder and will probably break horribly if used
