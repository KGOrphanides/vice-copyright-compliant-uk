This fork aims to make VICE 3.5 compliant with UK copyright law, under the terms of VICE's GNU General Public License (https://vice-emu.sourceforge.io/index.html#copyright).
To this end, firmware ROM images have been stripped out and a script has been provided to make it easy for users to integrate legally obtained ROMs. 

The Commdore firmware images are owned and licensed by Cloanto. 
The ROMs required by this VICE fork should be obtained as part of Cloanto's C64 Forever Express (https://www.c64forever.com/download/)

For convenience, Linux users are advised to install C64 Forever using Wine 5.0 or above, which has full MSI installer support (see https://www.winehq.org/news/2020012101). After installing this, locate /users/Public/Documents/CBM Files/Shared/rom/ of your Windows system drive.

Copy the rom directory to the vice-3.5-copyright-compliant directory and run copyrom.sh


*Steps to install*
Extract/clone vice-3.5-copyright-compliant
Install C64 Forever
Copy C64 Forever's /users/Public/Documents/CBM Files/Shared/rom/ directory into the vice-3.5-copyright-compliant directory
cd vice-3.5-copyright-compliant
sh copyrom.sh
make
sudo make install
